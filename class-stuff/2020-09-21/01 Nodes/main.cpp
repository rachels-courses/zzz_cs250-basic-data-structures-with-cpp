#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Node.hpp"

void OneNode()
{
    cout << endl << "ONE NODE" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string>* node = new Node<string>;
    node->data = "first";
    node->Display();
    delete node;
}

void TwoNodes()
{
    cout << endl << "TWO NODES" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string>* node1 = new Node<string>;
    Node<string>* node2 = new Node<string>;

    node1->data = "first";
    node2->data = "second";

    node1->ptrNext = node2;
    node2->ptrPrev = node1;

    node1->Display();
    node2->Display();

    delete node1;
    delete node2;
}

void ThreeNodes()
{
    cout << endl << "THREE NODES" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string>* node1 = new Node<string>;
    Node<string>* node2 = new Node<string>;
    Node<string>* node3 = new Node<string>;

    node1->data = "first";
    node2->data = "second";
    node3->data = "third";

    node1->ptrNext = node2;

    node2->ptrPrev = node1;
    node2->ptrNext = node3;

    node3->ptrPrev = node2;

    node1->Display();
    node2->Display();
    node3->Display();

    delete node1;
    delete node2;
    delete node3;
}

int main()
{
    OneNode();
    TwoNodes();
    ThreeNodes();

    return 0;
}
