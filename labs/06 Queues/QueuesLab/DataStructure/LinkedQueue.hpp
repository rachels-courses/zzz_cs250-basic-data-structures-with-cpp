#ifndef LINKED_QUEUE_HPP
#define LINKED_QUEUE_HPP

#include "LinkedList.hpp"

#include "../Utilities/Logger.hpp"
#include "../Exceptions/NotImplementedException.hpp"

template <typename T>
class LinkedQueue
{
public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T& Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

private:
    LinkedList<T> m_list;

    friend class QueueTester;
};

template <typename T>
void LinkedQueue<T>::Push(const T& newData )
{
    Logger::Out( "Function Begin", "LinkedQueue::Push" );

    throw NotImplementedException( "LinkedQueue::Push is not implemented" );
}

template <typename T>
void LinkedQueue<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "LinkedQueue::Pop" );

    throw NotImplementedException( "LinkedQueue::Pop is not implemented" );
}

template <typename T>
T& LinkedQueue<T>::Front()
{
    Logger::Out( "Function Begin", "LinkedQueue::Front" );

    throw NotImplementedException( "LinkedQueue::Front is not implemented" );
}

template <typename T>
int LinkedQueue<T>::Size()
{
    Logger::Out( "Function Begin", "LinkedQueue::Size" );

    throw NotImplementedException( "LinkedQueue::Front is not implemented" );
}

template <typename T>
bool LinkedQueue<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "LinkedQueue::IsEmpty" );

    throw NotImplementedException( "LinkedQueue::IsEmpty is not implemented" );
}

#endif
