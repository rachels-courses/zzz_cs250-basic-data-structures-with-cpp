#include "System.hpp"

//#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
//#include <Windows.h>
//#else
//#include <unistd.h>
//#endif

#include <chrono>
#include <thread>

void System::Sleep(int delay)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
}

void System::DisplayDirectoryContents( string path )
{
    string command;

    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    command = "dir ";
    #else
    command = "ls ";
    #endif

    command += path;

    system( command.c_str() );
}

/*
void System::CreateDirectory( const string& directory, bool relative )
{
    if ( relative )
    {
        CreateRelativeDirectory( directory );
    }
}

void System::CreateRelativeDirectory( const string& directory )
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        string cmd = "mkdir " + directory;
        system( cmd.c_str() );
    #else
        string cmd = "mkdir " + directory;
        system( cmd.c_str() );
    #endif
}
*/
