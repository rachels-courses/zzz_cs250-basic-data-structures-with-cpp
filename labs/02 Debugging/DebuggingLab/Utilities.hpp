#ifndef _UTILITIES_HPP
#define _UTILITIES_HPP

#include <string>
using namespace std;

void ClearScreen();
int GetIntInput( int minimum, int maximum );
string GetStringInput();

#endif
