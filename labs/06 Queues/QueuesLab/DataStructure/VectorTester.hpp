#ifndef _VECTOR_TESTER_HPP
#define _VECTOR_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "../cuTEST/TesterBase.hpp"
#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"

#include "Vector.hpp"

#include "../Exceptions/NotImplementedException.hpp"

class VectorTester : public TesterBase
{
public:
    VectorTester( string filename )
        : TesterBase( filename )
    {
        AddTest(TestListItem("VectorConstructor()", bind(&VectorTester::VectorConstructor, this)));
        AddTest(TestListItem("IsEmpty()",           bind(&VectorTester::IsEmpty, this)));
        AddTest(TestListItem("IsFull()",            bind(&VectorTester::IsFull, this)));
        AddTest(TestListItem("Size()",              bind(&VectorTester::Size, this)));

        AddTest(TestListItem("AllocateMemory()",    bind(&VectorTester::AllocateMemory, this)));
        AddTest(TestListItem("DeallocateMemory()",  bind(&VectorTester::DeallocateMemory, this)));
        AddTest(TestListItem("Resize()",            bind(&VectorTester::Resize, this)));

        AddTest(TestListItem("PushFront()",         bind(&VectorTester::PushFront, this)));
        AddTest(TestListItem("PushBack()",          bind(&VectorTester::PushBack, this)));
        AddTest(TestListItem("GetFront()",          bind(&VectorTester::GetFront, this)));
        AddTest(TestListItem("GetBack()",           bind(&VectorTester::GetBack, this)));
        AddTest(TestListItem("PopFront()",          bind(&VectorTester::PopFront, this)));
        AddTest(TestListItem("PopBack()",           bind(&VectorTester::PopBack, this)));
    }

    virtual ~VectorTester() { }

    friend class VectorTester;

private:
    int VectorConstructor();
    int IsEmpty();
    int IsFull();
    int Size();
    int AllocateMemory();
    int DeallocateMemory();
    int Resize();
    int PushFront();
    int PushBack();
    int GetFront();
    int GetBack();
    int PopFront();
    int PopBack();
};

int VectorTester::VectorConstructor()
{
    StartTestSet( "VectorTester::Vector Constructor", { } );
    Logger::Out( "Beginning test", "VectorTester::VectorConstructor" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;

            Set_ExpectedOutput  ( "Function implemented", true );
            Set_ActualOutput    ( "Function implemented", false );

            Logger::Out( "Constructor not implemented. Skipping the rest of this test set.", "VectorTester::VectorConstructor" );

            TestFail();
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_itemCount" );

        Vector<string> test;

        int expectedSize = 0;
        int actualSize = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", expectedSize );
        Set_ActualOutput    ( "m_itemCount", actualSize );

        if ( actualSize == expectedSize )
        {
            TestPass();
        }
        else
        {
            TestFail( "Bad m_itemCount value. Make sure you're setting the size in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    {
        /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_arraySize" );

        Vector<string> test;

        int expectedSize = 0;
        int actualSize = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedSize );
        Set_ActualOutput    ( "m_arraySize", actualSize );

        if ( actualSize == expectedSize )
        {
            TestPass();
        }
        else
        {
            TestFail( "Bad m_arraySize value. Make sure you're setting the size in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    {
        /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_data" );

        Vector<string> test;
        ostringstream oss;

        string* expected = nullptr;
        string* actual = test.m_data;

        oss << actual;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "nullptr" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actual == expected )
        {
            TestPass();
        }
        else
        {
            TestFail( "Bad m_data value. Make sure to set m_data to nullptr in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int VectorTester::IsEmpty()
{
    StartTestSet( "VectorTester::IsEmpty", { } );
    Logger::Out( "Beginning test", "VectorTester::IsEmpty" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if IsEmpty() has been implemented" );
        try
        {
            Vector<string> vec;
            vec.IsEmpty();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "IsEmpty() not implemented. Skipping the rest of this test set.", "VectorTester::IsEmpty" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that IsEmpty() returns true." );

        Vector<string> test;

        bool expectedResult = true;
        bool actualResult = test.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedResult );
        Set_ActualOutput    ( "IsEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector and add item. Check that IsEmpty() returns false." );

        Vector<string> test;
        test.m_itemCount = 1;

        bool expectedResult = false;
        bool actualResult = test.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedResult );
        Set_ActualOutput    ( "IsEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::IsFull()
{
    StartTestSet( "VectorTester::IsFull", { "Push()", "AllocateMemory()" } );
    Logger::Out( "Beginning test", "VectorTester::IsFull" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.IsFull();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "IsFull() not implemented. Skipping the rest of this test set.", "VectorTester::IsFull" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create empty array, check IsFull() returns true." );

        Vector<string> test;

        bool expectedResult = true;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );
        Set_Comments( "When first created, no memory is allocated and the size and item count should be 0." );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector, set item count and array to the different values, check IsFull() returns false." );

        Vector<string> test;
        test.m_arraySize = 5;
        test.m_itemCount = 3;

        bool expectedResult = false;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector, set item count and array to same values, check IsFull() returns false." );

        Vector<string> test;
        test.m_arraySize = 5;
        test.m_itemCount = 5;

        bool expectedResult = true;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::Size()
{
    StartTestSet( "VectorTester::Size", { "AllocateMemory()", "Push()", "Resize()" } );
    Logger::Out( "Beginning test", "VectorTester::Size" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.Size();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "Size() not implemented. Skipping the rest of this test set.", "VectorTester::Size" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create empty Vector. Check that size is 0." );

        Vector<string> test;

        int expectedResult = 0;
        int actualResult = test.Size();

        Set_ExpectedOutput  ( "Size()", expectedResult );
        Set_ActualOutput    ( "Size()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE AllocateMemory() has been implemented" );
        Set_Comments( "Next test requires the AllocateMemory() function." );
        try
        {
            Vector<string> vec;
            vec.AllocateMemory( 10 );
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that size is 5." );

        Vector<string> test;
        test.m_itemCount = 5;

        int expectedResult = 5;
        int actualResult = test.Size();

        Set_ExpectedOutput  ( "Size()", expectedResult );
        Set_ActualOutput    ( "Size()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::AllocateMemory()
{
    StartTestSet( "VectorTester::AllocateMemory", { } );
    Logger::Out( "Beginning test", "VectorTester::AllocateMemory" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.AllocateMemory( 10 );
            if ( vec.m_data != nullptr )
            {
                delete [] vec.m_data;
                vec.m_data = nullptr;
            }
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "AllocateMemory() not implemented. Skipping the rest of this test set.", "VectorTester::AllocateMemory" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data is pointing to an address after AllocateMemory()." );
        ostringstream oss;

        Vector<string> test;
        test.AllocateMemory( 10 );

        string* actualResult = test.m_data;

        oss << actualResult;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "ANY non-null address" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actualResult == nullptr )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize is set after AllocateMemory()" );

        Vector<string> test;
        test.AllocateMemory( 10 );

        int expectedResult = 10;
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedResult );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::DeallocateMemory()
{
    StartTestSet( "VectorTester::DeallocateMemory", {  } );
    Logger::Out( "Beginning test", "VectorTester::DeallocateMemory" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.m_data = new string[5];
            vec.DeallocateMemory();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "DeallocateMemory() not implemented. Skipping the rest of this test set.", "VectorTester::DeallocateMemory" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data is pointing to nullptr after DeallocateMemory()." );
        ostringstream oss;

        Vector<string> test;
        test.m_data = new string[10];
        test.DeallocateMemory();

        string* expectedResult = nullptr;
        string* actualResult = test.m_data;

        oss << actualResult;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "nullptr" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize is set to 0 after DeallocateMemory()" );

        Vector<string> test;
        test.m_data = new string[10];
        test.DeallocateMemory();

        int expectedResult = 0;
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedResult );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::Resize()
{
    StartTestSet( "VectorTester::Resize", { } );
    Logger::Out( "Beginning test", "VectorTester::Resize" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.Resize();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "Resize() not implemented. Skipping the rest of this test set.", "VectorTester::Resize" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data address is different after Resize()." );
        ostringstream oss;

        Vector<string> test;
        test.m_data = new string[10];
        test.m_arraySize = 10;

        string* oldAddress = test.m_data;

        test.Resize();
        string* actualResult = test.m_data;

        oss << oldAddress;

        Set_ExpectedOutput  ( "m_data points to", string( "anything BESIDES " + oss.str() ) );

        oss << actualResult;
        Set_ActualOutput    ( "m_data points to", string( oss.str() ) );

        if ( actualResult == oldAddress )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize increases after Resize()" );

        Vector<string> test;
        test.m_data = new string[10];
        test.m_arraySize = 10;

        int oldSize = test.m_arraySize;

        test.Resize();
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", string( "Anything above " + StringUtil::ToString( oldSize ) ) );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult == oldSize )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_itemCount stays the same after Resize()" );

        Vector<string> test;
        test.m_data = new string[10];
        test.m_arraySize = 10;
        test.m_itemCount = 5;

        int oldSize = test.m_itemCount;

        test.Resize();
        int actualResult = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", oldSize );
        Set_ActualOutput    ( "m_itemCount", actualResult );

        if ( actualResult != oldSize )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check that items are still in vector after Resize()" );

        Vector<string> test;
        test.m_data = new string[4];
        test.m_arraySize = 4;
        test.m_itemCount = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";
        test.Resize();

        string expectedResult[] = { "A", "B", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::PushFront()
{
    StartTestSet( "VectorTester::PushFront", { } );
    Logger::Out( "Beginning test", "VectorTester::PushFront" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.PushFront("Hi");
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "PushFront() not implemented. Skipping the rest of this test set.", "VectorTester::PushFront" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if Push adds items to vector. PushFront: A, B, C, D" );

        Vector<string> test;
        test.m_data = new string[5];

        test.PushFront( "A" );
        test.PushFront( "B" );
        test.PushFront( "C" );
        test.PushFront( "D" );

        string expectedResult[] = { "D", "C", "B", "A" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::PushBack()
{
    StartTestSet( "VectorTester::PushBack", { } );
    Logger::Out( "Beginning test", "VectorTester::PushBack" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.PushBack("Hi");
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "PushBack() not implemented. Skipping the rest of this test set.", "VectorTester::PushBack" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if Push adds items to vector. PushBack: A, B, C, D" );

        Vector<string> test;
        test.m_data = new string[5];

        test.PushBack( "A" );
        test.PushBack( "B" );
        test.PushBack( "C" );
        test.PushBack( "D" );

        string expectedResult[] = { "A", "B", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::GetFront()
{
    StartTestSet( "VectorTester::GetFront", { } );
    Logger::Out( "Beginning test", "VectorTester::GetFront" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.m_data = new string[10];
            vec.m_itemCount = 10;
            vec.m_arraySize = 10;
            vec.GetFront();
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "GetFront not implemented. Skipping the rest of this test set.", "VectorTester::GetFront" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Get front" );

        Vector<string> test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        string expectedResult = "A";
        string actualResult = test.GetFront();

        Set_ExpectedOutput  ( "GetFront()", expectedResult );
        Set_ActualOutput    ( "GetFront()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::GetBack()
{
    StartTestSet( "VectorTester::GetBack", { } );
    Logger::Out( "Beginning test", "VectorTester::GetBack" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.m_data = new string[10];
            vec.m_itemCount = 10;
            vec.m_arraySize = 10;
            vec.GetFront();
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "GetBack not implemented. Skipping the rest of this test set.", "VectorTester::GetBack" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Get back" );

        Vector<string> test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        string expectedResult = "D";
        string actualResult = test.GetBack();

        Set_ExpectedOutput  ( "GetBack()", expectedResult );
        Set_ActualOutput    ( "GetBack()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}


int VectorTester::PopFront()
{
    StartTestSet( "VectorTester::PopFront", { } );
    Logger::Out( "Beginning test", "VectorTester::PopFront" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.PopFront();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "PopFront not implemented. Skipping the rest of this test set.", "VectorTester::PopFront" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Add A, B, C, D, then Pop front" );

        Vector<string> test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";
        test.PopFront();

        string expectedResult = "B";
        string actualResult = test.m_data[0];

        Set_ExpectedOutput  ( "First item", expectedResult );
        Set_ActualOutput    ( "First item", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int VectorTester::PopBack()
{
    StartTestSet( "VectorTester::PopBack", { } );
    Logger::Out( "Beginning test", "VectorTester::PopBack" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector<string> vec;
            vec.PopBack();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();

            Logger::Out( "PopBack not implemented. Skipping the rest of this test set.", "VectorTester::PopBack" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Add A, B, C, D, then Pop back" );

        Vector<string> test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";
        test.PopBack();

        string expectedResult = "C";
        string actualResult = test.m_data[test.m_itemCount-1];

        Set_ExpectedOutput  ( "Last item", expectedResult );
        Set_ActualOutput    ( "Last item", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

#endif
