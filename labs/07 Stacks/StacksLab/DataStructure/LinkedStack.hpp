#ifndef LINKED_QUEUE_HPP
#define LINKED_QUEUE_HPP

#include "LinkedList.hpp"

#include "../Utilities/Logger.hpp"
#include "../Exceptions/NotImplementedException.hpp"

template <typename T>
class LinkedStack
{
public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T& Top();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

private:
    LinkedList<T> m_list;

    friend class StackTester;
};

template <typename T>
void LinkedStack<T>::Push(const T& newData )
{
    Logger::Out( "Function Begin", "LinkedStack::Push" );

    throw NotImplementedException( "ArrayQueue::Push is not implemented" );
}

template <typename T>
void LinkedStack<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "LinkedStack::Pop" );

    throw NotImplementedException( "ArrayQueue::Pop is not implemented" );
}

template <typename T>
T& LinkedStack<T>::Top()
{
    Logger::Out( "Function Begin", "LinkedStack::Front" );

    throw NotImplementedException( "ArrayQueue::Front is not implemented" );
}

template <typename T>
int LinkedStack<T>::Size()
{
    Logger::Out( "Function Begin", "LinkedQueue::Size" );

    throw NotImplementedException( "ArrayQueue::Size is not implemented" );
}

template <typename T>
bool LinkedStack<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "LinkedQueue::IsEmpty" );

    return m_list.IsEmpty();
}

#endif
