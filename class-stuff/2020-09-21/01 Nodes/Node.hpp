#ifndef _NODE_HPP
#define _NODE_HPP

#include <iomanip>
#include <iostream>
using namespace std;

template <typename T>
class Node
{
    public:
    Node()
    {
        ptrPrev = nullptr;
        ptrNext = nullptr;
    }

    Node* ptrPrev;
    Node* ptrNext;
    T data;

    void Display()
    {
        cout << left
            << setw( 10 ) << data
            << setw( 20 ) << ptrPrev
            << setw( 20 ) << ptrNext << endl;
    }
};



#endif
