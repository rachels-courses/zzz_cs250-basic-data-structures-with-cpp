#ifndef _STRINGS
#define _STRINGS

#include <string>
#include <sstream>
using namespace std;

class StringUtil
{
    public:
    template <typename T>
    static string ToString( const T& value );
    static string BoolToString( bool value );
    static int StringToInt( const string& str );
    static string ToUpper( const string& val );
    static string ToLower( const string& val );
    static string ColumnText( int colWidth, const string& text );
    static bool Contains( string haystack, string needle );
};

template <typename T>
string StringUtil::ToString( const T& value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}

#endif
