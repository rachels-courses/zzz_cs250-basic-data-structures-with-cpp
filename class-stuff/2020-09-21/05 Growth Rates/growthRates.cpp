#include <iostream>
#include <iomanip>
#include <locale>
#include <string>
using namespace std;

int Constant_F( int x, int& iterations )
{
    iterations++;
    return 3 * x + 2;
}

int Logarithmic_Search( int l, int r, int search, int arr[], int& iterations )
{
    while ( l <= r )
    {
        iterations++;
        int m = l + (r-l) / 2;
        if      ( arr[m] == search )    { return m; }
        else if ( arr[m] < search )     { l = m+1; }
        else if ( arr[m] > search )     { r = m-1; }
    }
    return -1;
}

int Linear_Sum( int n, int& iterations )
{
    int sum = 0;
    for ( int i = 1; i <= n; i++ )
    {
        iterations++;
        sum += n;
    }
    return sum;
}

int Quadratic_TimesTables( int n, int& iterations )
{
    for ( int y = 1; y <= n; y++ )
    {
        for ( int x = 1; x <= n; x++ )
        {
            iterations++;
            // Commented out to not mess up
            // my nice table below
            //cout << x << "*" << y << "=" << (x*y) << "\t";
        }
        //cout << endl;
    }
    return iterations;
}

int Cubic_Thing( int n, int& iterations )
{
    for ( int z = 1; z <= n; z++ )
    {
        for ( int y = 1; y <= n; y++ )
        {
            for ( int x = 1; x <= n; x++ )
            {
                iterations++;
            }
        }
    }
    return iterations;
}

int Exponential_Fib( int n, int& iterations )
{
    if ( n == 0 || n == 1 ) { iterations++; return 1; }
    iterations++;
    return
        Exponential_Fib( n-2, iterations )
        + Exponential_Fib( n-1, iterations );
}

void Helper_FillArray( int arr[], int n )
{
    for ( int i = 0; i < n; i++ )
    {
        arr[i] = n * 10;
    }
}

int main()
{
    const int COL_T = 15;
    const int COL_N = 5;
    const int COL_R = 12;
    const int COL_I = 12;

    const int n = 20;

    // http://cplusplus.com/forum/beginner/112656/
    cout.imbue( locale( "en_US.utf8" ) );

    cout << left
        << setw( COL_T ) << "type"
        << setw( COL_N ) << "n"
        << setw( COL_R ) << "result"
        << setw( COL_I ) << "iterations" << endl;
    cout << "------------------------------------------------------" << endl;

    int constIterations = 0;
    int constResult = Constant_F( n, constIterations );
    cout << left
        << setw( COL_T ) << "Constant"
        << setw( COL_N ) << n
        << setw( COL_R ) << constResult
        << setw( COL_I ) << constIterations << endl;


    int logIterations = 0;
    int arr[n];
    Helper_FillArray( arr, n );
    int logResult = Logarithmic_Search( 0, n, -50, arr, logIterations );
    cout << left
        << setw( COL_T ) << "Logarithmic"
        << setw( COL_N ) << n
        << setw( COL_R ) << logResult
        << setw( COL_I ) << logIterations << endl;


    int linearIterations = 0;
    int linearResult = Linear_Sum( n, linearIterations );
    cout << left
        << setw( COL_T ) << "Linear"
        << setw( COL_N ) << n
        << setw( COL_R ) << linearResult
        << setw( COL_I ) << linearIterations << endl;


    int quadraticIterations = 0;
    int quadraticResult = Quadratic_TimesTables( n, quadraticIterations );
    cout << left
        << setw( COL_T ) << "Quadratic"
        << setw( COL_N ) << n
        << setw( COL_R ) << quadraticResult
        << setw( COL_I ) << quadraticIterations << endl;


    int cubicIterations = 0;
    int cubicResult = Cubic_Thing( n, cubicIterations );
    cout << left
        << setw( COL_T ) << "Cubic"
        << setw( COL_N ) << n
        << setw( COL_R ) << cubicResult
        << setw( COL_I ) << cubicIterations << endl;


    int exponentialIterations = 0;
    int exponentialResult = Exponential_Fib( n, exponentialIterations );
    cout << left
        << setw( COL_T ) << "Exponential"
        << setw( COL_N ) << n
        << setw( COL_R ) << exponentialResult
        << setw( COL_I ) << exponentialIterations << endl;

    return 0;
}
