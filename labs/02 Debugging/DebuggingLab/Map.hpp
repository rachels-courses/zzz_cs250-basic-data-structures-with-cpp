#ifndef _MAP_HPP
#define _MAP_HPP

#include <string>
using namespace std;

struct Coords
{
    int x, y;
};

class Map
{
    public:
    Map();
    void GenerateMap();
    void Draw();
    void MovePlayer( string direction );
    void MoveGoblin();
    bool PlayerCollectTreasure();
    bool GoblinGetPlayer();

    char MoveUpKey();
    char MoveDownKey();
    char MoveLeftKey();
    char MoveRightKey();

    private:
    const int MAP_WIDTH;
    const int MAP_HEIGHT;
    const char EMPTYROOM;
    const char WALL;
    const char MOVE_UP;
    const char MOVE_DOWN;
    const char MOVE_LEFT;
    const char MOVE_RIGHT;

    char m_tiles[20][10];
    Coords m_player;
    Coords m_treasure;
    Coords m_enemy;
};

#endif
