#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

#include "Vector.hpp"

#include "../Utilities/Logger.hpp"
#include "../Exceptions/NotImplementedException.hpp"

template <typename T>
class ArrayQueue
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T& Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    Vector<T> m_vector;

    friend class QueueTester;
};

template <typename T>
void ArrayQueue<T>::Push( const T& newData )
{
    Logger::Out( "Function Begin", "ArrayQueue::Push" );

    m_vector.PushBack( newData );
}

template <typename T>
void ArrayQueue<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "ArrayQueue::Pop" );

    throw NotImplementedException( "ArrayQueue::Pop is not implemented" );
}

template <typename T>
T& ArrayQueue<T>::Front()
{
    Logger::Out( "Function Begin", "ArrayQueue::Front" );

    throw NotImplementedException( "ArrayQueue::Front is not implemented" );
}

template <typename T>
int ArrayQueue<T>::Size()
{
    Logger::Out( "Function Begin", "ArrayQueue::Size" );

    return m_vector.Size();
}

template <typename T>
bool ArrayQueue<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "ArrayQueue::IsEmpty" );

    throw NotImplementedException( "ArrayQueue::IsEmpty is not implemented" );
}

#endif
