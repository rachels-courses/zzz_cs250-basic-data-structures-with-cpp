#include <iostream>
using namespace std;

int GetFib_Iterative( int n, int& iters )
{
    if ( n == 0 || n == 1 )
    {
        return 1;
    }

    int n_prev2 = 1;    // F[n-2]
    int n_prev1 = 1;    // F[n-1]
    int ni;             // F[n]

    for ( int i = 2; i <= n; i++ )
    {
        // F[n] = F[n-2] + F[n-1]
        ni = n_prev2 + n_prev1;

        // F[n-2] = F[n-1]
        n_prev2 = n_prev1;

        // F[n-1] = F[n]
        n_prev1 = ni;

        // Keep track of amount of iterations
        iters++;
    }

    return ni;
}

int GetFib_Recursive( int n, int& iters )
{
    if ( n == 0 || n == 1 ) { return 1; }
    else
    {
        iters++;
        // F[n] = F[n-2] + F[n-1]
        int ni = GetFib_Recursive( n-2, iters ) + GetFib_Recursive( n-1, iters );
        return ni;
    }
}

int main()
{
    bool done = false;
    while ( !done )
    {
        int iterativeIterations = 0;
        int recursiveIterations = 0;

        int n;
        cout << "Enter n: ";
        cin >> n;

        cout << GetFib_Iterative( n, iterativeIterations )
        << "\t Total iterations: " << iterativeIterations << endl;

        cout << GetFib_Recursive( n, recursiveIterations )
        << "\t Total iterations: " << recursiveIterations << endl;

    }

    return 0;
}
