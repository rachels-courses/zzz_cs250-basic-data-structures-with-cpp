#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

#include "Vector.hpp"

#include "../Utilities/Logger.hpp"
#include "../Exceptions/NotImplementedException.hpp"

template <typename T>
class ArrayStack
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T& Top();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    Vector<T> m_vector;

    friend class StackTester;
};

template <typename T>
void ArrayStack<T>::Push( const T& newData )
{
    Logger::Out( "Function Begin", "ArrayQueue::Push" );

    throw NotImplementedException( "ArrayQueue::Push is not implemented" );
}

template <typename T>
void ArrayStack<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "ArrayQueue::Pop" );

    throw NotImplementedException( "ArrayQueue::Pop is not implemented" );
}

template <typename T>
T& ArrayStack<T>::Top()
{
    Logger::Out( "Function Begin", "ArrayQueue::Front" );

    throw NotImplementedException( "ArrayQueue::Front is not implemented" );
}

template <typename T>
int ArrayStack<T>::Size()
{
    Logger::Out( "Function Begin", "ArrayQueue::Size" );

    throw NotImplementedException( "ArrayQueue::Size is not implemented" );
}

template <typename T>
bool ArrayStack<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "ArrayQueue::IsEmpty" );

    return m_vector.IsEmpty();
}

#endif
